from smartbuy import views
from django.conf.urls import url, include

urlpatterns = [
	url(r'^search/general/(?P<product>[a-zA-Z]+)/(?P<address>[a-zA-Z0-9]+(-[a-zA-Z0-9]+)+(-[a-zA-Z0-9]+)|[a-zA-Z0-9]+(-[a-zA-Z0-9]+)+|[a-zA-Z0-9]+)/$', views.search_general),
	url(r'^search/detail/$', views.search_detail),
	url(r'^place/(?P<place>[a-zA-Z]+)/(?P<ltlng>(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?))$', views.search_place),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]