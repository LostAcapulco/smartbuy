#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import sys
import time
import unicodedata
import inspect
import settings
import telepot
import telepot.namedtuple

from  telepot.routing import by_command as by_command
from telepot.namedtuple import KeyboardButton, ReplyKeyboardMarkup
from selenium_agent.call_profecho_agent import CallAgent as AgentCall

class SmartBot(object):
	"""docstring for SmartBot"""
	def __init__(self):
		super(SmartBot, self).__init__()
		self.smart_bot = telepot.Bot(settings.TOKEN)
		self.location = False
		self.latitude_longuitude = {'latitude':'','longuitude':''}
		
	def show_instructions(self):
		"""Show the instructions about how this bot works"""
		instructions = 'Smartbuybot puede buscar el precio '
		instructions += 'más barato de un producto dentro de tu zona. \n'
		instructions += 'Así como lugares cercanos a ti como "cafeterias" o "tiendas"\n'
		instructions += '\nPara iniciar debes mandar tu ubicacion y a continuacion el mensaje:\n\n'
		instructions += '/buscar | nombre_del_producto\n\n'
		instructions += 'Nuestro bot te mandara una lista con los productos'
		instructions += ' relacionados con tu busqueda.\n'
		instructions += '\n'
		instructions += 'A continuacion envia el mensaje:\n\nprecio | nombre_del_producto\n\n'
		instructions += 'Para conocer las tiendas que ofrecen ese producto y sus precios.\n'
		instructions += '\n'
		instructions += 'Para buscar algun lugar cercano usa el comando.\n'
		instructions += '/lugar | starbucks\n'
		return instructions

	def start_presentation(self):
		"""Prepare text a buttons to show in the first interact with the bot"""
		location_button = KeyboardButton(text='Enviar ubicacion', 
										request_contact=None, 
										request_location=True)
		self.reply_keyboard = ReplyKeyboardMarkup(keyboard=[[location_button]], 
											one_time_keyboard=True)
		self.smart_bot.sendMessage(settings.CHAT_ID,'Hola %s'%(settings.user_data['name']))
		self.smart_bot.sendMessage(settings.CHAT_ID,self.show_instructions())
		self.smart_bot.sendMessage(settings.CHAT_ID,'Evianos tu ubicacion',reply_markup=self.reply_keyboard)

	def on_chat_message(self,msg):
		"""Handle the messages on a chat"""
		self.load_user_data(msg)
		keys, text, command, param = self.search_keys(msg)
		self.execute_command(command,param)
		self.smart_bot.getUpdates()

	def load_user_data(self,msg):
		"""Parse the message on a chat"""
		settings.CHAT_CONTENT_TYPE, settings.CHAT_TYPE, settings.CHAT_ID = telepot.glance(msg)
		settings.user_data['name'] = msg['from']['first_name']
		settings.user_data['last_name'] = msg['from']['last_name']
		settings.user_data['id'] = msg['from']['id']
		
	def execute_command(self,command,param):
		"""Execute commands sent by the user"""
		print command, param
		if self.location and not command:
			self.smart_bot.sendMessage(settings.CHAT_ID,'Que producto deseas buscar:')
			self.smart_bot.sendMessage(settings.CHAT_ID,'/buscar | Mi producto ')
		if command == 'start' or not self.location:
			self.start_presentation()
		if command.__str__() == 'buscar' and len(param) != 0:
			self.smart_bot.sendMessage(settings.CHAT_ID,'Estamos buscando '%(param))
			self.smart_bot.sendMessage(settings.CHAT_ID,'Espera un momento por favor...')
		elif param is not None and len(param) == 0:
			self.smart_bot.sendMessage(settings.CHAT_ID,'es necesario que indiques que producto buscas')
		if command.__str__() == 'localizar':
			self.smart_bot.sendMessage(settings.CHAT_ID,'Estamos buscando '%(param))
			self.smart_bot.sendMessage(settings.CHAT_ID,'Espera un momento por favor...')
		if command.__str__() == 'lugar':
			self.smart_bot.sendMessage(settings.CHAT_ID,'Estamos buscando '%(param))
			self.smart_bot.sendMessage(settings.CHAT_ID,'Espera un momento por favor...')
			nearest_place = self.search_place()
			result = 'Resultados:\n'
			result += nearest_place
			self.smart_bot.sendMessage(settings.CHAT_ID,result)


	def search_product(self,param):
		"""""Search a product using a profeco agent"""
		address = "Ciudad de Mexico"
		products = AgentCall(product=param,address=address).general_serach()
		return products

	def search_product_detail(self,product,address,description,):
		"""Search a especific product"""
		product_places = AgentCall().detail_search(address=address,
                                           description=description,
                                           product_detail=product)
		return product_places

	def search_place(self,param):
		"""Search for a nearest place"""
		ltlng = self.latitude_longuitude['latitude']+','+self.latitude_longuitude['longitude']
		nearest_place = AgentCall().search_place(param, ltlng)
		return nearest_place

	def search_keys(self,msg):
		"""Search keys in the message"""
		keys, command, param, text_sended = [], None, None, None
		for i in msg:
			if 'from' in msg:
				keys.append('from')
			if 'text' in msg:
				keys.append('text')
				command, param = self.get_command(msg)
				if command == None:
					text_sended = msg['text']
			if 'chat' in msg:
				keys.append('chat')
			if 'reply_to_message' in msg:
				keys.append('reply_to_message')
			if 'location' in msg:
				keys.append('location')
				self.latitude_longuitude = msg['location']
				self.location = True
		return keys, text_sended, command, param

	def get_command(self,msg, prefix=['/'], separator='|', pass_args=True):
		"""Manage command in the message"""
		text = msg['text']
		for px in prefix:
			if text.startswith(px):
				chunks = text[len(px):].split(separator)
				return unicodedata.normalize('NFKD', chunks[0])\
				       .encode('ascii','ignore'), chunks[1:] if pass_args else None
			else:
				return None, None

	def search_text(self,mgs):
		"""Search text in the mesage"""
		pass

	def store_data(self,*args,**kwargs):
		"""Store in data base"""
		pass

	def on_callback_query(self,msg):
		"""Show a message that response a button callback"""
		query_id, from_id, query_data = telepot.glance(msg, flavor='callback_query')
		print('Callback Query:', query_id, from_id, query_data)
		self.smart_bot.answerCallbackQuery(query_id, text='Got it')

	def run(self):
		"""Start the bot execution"""
		self.smart_bot.message_loop({'chat': self.on_chat_message,
	                      'callback_query': self.on_callback_query})
		print('listening...')
		while 1:
			time.sleep(10)

if __name__ == '__main__':
				
	SmartBot().run()