#!/usr/bin/python
# -*- encoding: utf-8 -*-
from selenium.webdriver.common.keys import Keys
from agent import Agent


class Agent_detail(Agent):
	"""docstring for Agent_detail"""
	def __init__(self, *args):
		super(Agent_detail, self).__init__()
		self.city = args[0]
		self.municipio = args[1]
		self.search = args[2]
		self.url = args[3]
		self.description = args[4]

	def _aux_driver(self, url_search):
		self._create_agent()
		self.url_search = url_search
		self._go_to_search()

	def get_detail(self, flag=False, table_id="grdEstProd"):
		product = []
		data_list = []
		
		self._start_agent()
		self.url_search = self.url
		self._go_to_search()
		
		table = self.driver.find_element_by_id(table_id)
		tr_elements = table.find_elements_by_xpath(".//tr")
		
		for tr_element in tr_elements:
			td_elements = tr_element.find_elements_by_tag_name("td")
			
			if flag:
				data_dictionary = {}
				data_dictionary.update({'Establecimiento':td_elements[0].text,
										'Precio':td_elements[1].text})
				address_link = td_elements[0].find_elements_by_tag_name("a")
				link_address = address_link[0].get_attribute('href')

				self.driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + 't')
				self.driver.switch_to.window(self.driver.window_handles[-1]) 
				self.driver.get(link_address)

				store_address = self.driver.find_element_by_id("lblDireccion")
				data_dictionary.update({'direccion':store_address.text})
				data_list.append(data_dictionary)

				self.driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + 'w')
				self.driver.switch_to.window(self.driver.window_handles[0])
			else:
				flag = True
		
		self._quit_agent()
		
		return [{'name':self.description,'places':data_list}]
"""
if __name__ == '__main__':
	agent = Agent_detail('150901',
						'1509010',
						'atun',
						'http://www.profeco.gob.mx/precios/canasta/listaEstProd.aspx?cve_prodmarca=0136129',
						'ATUN, DOLORES, LATA 140 GR. EN AGUA. LOMO')
	print agent.get_detail()
"""