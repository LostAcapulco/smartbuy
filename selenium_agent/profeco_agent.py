#!/usr/bin/python
# -*- encoding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.select import Select

#from django.conf import settings
class settings(object):
	"""docstring for settings"""
	def __init__(self, arg):
		super(settings, self).__init__()
		self.arg = arg
	PROFECO_APP_URL_LOGIN = 'http://www.profeco.gob.mx/precios/canasta/loginUsu.aspx'
	PROFECO_APP_URL_SEARCH = 'http://www.profeco.gob.mx/precios/canasta/home.aspx?th=1'
	PROFECO_USERNAME = 'jl.mrtz@gmail.com'
	PROFECO_PASSWORD = 'Xilcom12'

import logging
import time
import threading

logging.basicConfig(filename='test_debug.log',level=logging.DEBUG)

class SeleniumProfeco(object):

	def __init__(self,*args): 
		self.city = args[0]
		self.municipio = args[1]
		self.search = args[2]
		self.products_list = []

	def run_agent(self):
		"""Run new Agent for new reach inside of the instance"""
		#print self.run_agent.__doc__, "\n"
		agent = self.Agent(self.city, self.municipio, self.search)
		agent._create_agent()
		agent.select_city()
		agent.select_municipio()
		agent.do_search()
		total_results = agent.count_results()
		products = agent.get_all_results()
		print products
		agent.quit_agent()
		for i in range(total_results):
			agent = self.Agent(self.city, self.municipio, self.search)
			thread = self.ThreadDataProduct(kwargs={'products':products, 
											'index':i,
											'table_id':'grdEstProd',
											'flag':False,
											'agent':agent,
											'callback_append':self.callback_append})
			thread.start()
			#print i
		return self.products_list

	def callback_append(self,products=None):
		"""Append products on a list for a product detail given"""
		self.products_list.append(products)

	class Agent(object):
		"""Execute Agent for agent"""
		def __init__(self, *args):
			self.url_login = settings.PROFECO_APP_URL_LOGIN
			self.url = settings.PROFECO_APP_URL_SEARCH
			self.city = args[0]
			self.municipio = args[1]
			self.search = args[2]
			self.frame_options = ''
			self.frame_results = ''


		def _create_agent(self,driver_type='firefox'):
			"""Create a new aget"""
			#print self._create_agent.__doc__
			if driver_type == 'firefox':
				self.driver = webdriver.Firefox()
				self.driver_two = webdriver.Firefox()
			elif driver_type == 'headless':
				self.driver = webdriver.PhantomJS(service_log_path=settings.PATH_LOG_PHANTOMJS)
				self.driver_two = webdriver.PhantomJS(service_log_path=settings.PATH_LOG_PHANTOMJS)
			self.driver_two.get(self.url_login)
			self.sigin_agent_two()
			self.driver.get(self.url_login)
			self.sigin_agent_one()
			self.driver.get(self.url)
			self.wait = WebDriverWait(self.driver,3)
			return self.driver, self.driver_two

		def sigin_agent_one(self):
			"""Log in agent for a search on detail product"""
			self.driver.find_element_by_id("txtUsuario").clear()
			self.driver.find_element_by_id("txtUsuario").send_keys(settings.PROFECO_USERNAME)
			self.driver.find_element_by_id("txtPassword").clear()
			self.driver.find_element_by_id("txtPassword").send_keys(settings.PROFECO_PASSWORD)
			self.driver.find_element_by_id("btnEnviar").click()

		def sigin_agent_two(self):
			"""Log in agent for a search on detail prouct"""
			self.driver_two.find_element_by_id("txtUsuario").clear()
			self.driver_two.find_element_by_id("txtUsuario").send_keys(settings.PROFECO_USERNAME)
			self.driver_two.find_element_by_id("txtPassword").clear()
			self.driver_two.find_element_by_id("txtPassword").send_keys(settings.PROFECO_PASSWORD)
			self.driver_two.find_element_by_id("btnEnviar").click()
			
		def select_city(self, select_id="cmbCiudad"):
			"""Select the city of the list"""
			#print self.select_city.__doc__
			self.frame_options = self.driver.find_elements_by_tag_name('iframe')[0]
			self.driver.switch_to.frame(self.frame_options);
			self.wait.until(lambda driver: self.driver.find_element_by_id(select_id))
			combo_city = self.driver.find_element_by_id(select_id)
			Select(combo_city).select_by_value(self.city)

		def select_municipio(self, select_id="listaMunicipios", button_id="ImageButton1"):
			"""Select the municipio of the list"""
			#print self.select_municipio.__doc__
			self.wait.until(lambda driver: self.driver.find_element_by_id(select_id))
			select = self.driver.find_element_by_id(select_id)
			Select(select).select_by_value(self.municipio)
			button = self.driver.find_element_by_id(button_id)
			button.click()

		def do_search(self, input_id="busq"):
			"""Make a search for product"""
			#print self.do_search.__doc__
			time.sleep(3)
			input_text = self.driver.find_element_by_id(input_id)
			input_text.send_keys(self.search)
			button_search = self.driver.find_element_by_id("IMG1")
			button_search.click()

		def count_results(self, total_results=0, table_id="GridView1"):
			"""Count the results for a product search"""
			#print self.count_results.__doc__
			data_list, td_content = [], []
			self.driver.switch_to.default_content()
			self.frame_results = self.driver.find_elements_by_tag_name('iframe')[1]
			self.driver.switch_to.frame(self.frame_results);
			
			table = self.driver.find_element_by_id(table_id)
			tr_elements = table.find_elements_by_xpath(".//tr")
			for tr_element in tr_elements:
				td_elements = tr_element.find_elements_by_tag_name("td")
				td_content.append(td_elements)
			del td_content[0]
			total_results = len(td_content)
			return total_results

		def get_all_results(self, table_id="GridView1", response=None,flag=False):
			"""Extrac the results from the table."""
			#print self.get_all_results.__doc__
			data_list, td_content = [], []
			self.driver.switch_to.default_content()
			self.driver.switch_to.frame(self.frame_results);
			table = self.driver.find_element_by_id(table_id)
			tr_elements = table.find_elements_by_xpath(".//tr")
			for tr_element in tr_elements:
				td_elements = tr_element.find_elements_by_tag_name("td")

				if flag:
					data_dictionary = {}
					description = td_elements[2].find_elements_by_tag_name("a")
					url = description[0].get_attribute('href')
					data_dictionary.update({'url':url,
										'description':description[0].text,
										'preciomin':td_elements[3].text,
										'preciomax':td_elements[4].text})
					data_list.append(data_dictionary)
				else:
					flag = True
			return data_list

		def quit_agent(self):
			"""Quit all agents"""
			self.driver.quit()
			self.driver_two.quit()
			self.__del__()

		def __del__(self):
			pass

	class ThreadDataProduct(threading.Thread):

		def __init__(self, group=None, target=None, name=None,
							args=None, kwargs=None, verbose=None):
			threading.Thread.__init__(self, group=group, target=target, name=name,
										verbose=verbose)
			self.args = args
			self.kwargs = kwargs
			self.agent = kwargs['agent']
			self.driver, self.driver_two = self.agent._create_agent()
			self.agent.select_city()
			self.agent.select_municipio()
			self.callback_append = kwargs['callback_append']
			return

		def run(self):
			"""
			Get the detail info about a list products
			"""
			#print self.run.__doc__
			products = self.kwargs['products']
			data_list, td_content = [], []
			if products is not None:
				try:
					current_window = self.driver.current_window_handle
					product = products[self.kwargs['index']]
					result_link = product['url']
					self.driver.get(result_link)
					table = self.driver.find_element_by_id(self.kwargs['table_id'])
					tr_elements = table.find_elements_by_xpath(".//tr")
					for tr_element in tr_elements:
						td_elements = tr_element.find_elements_by_tag_name("td")
						td_content.append(td_elements)
						if self.kwargs['flag']:
							data_dictionary = {}
							data_dictionary.update({'Establecimiento':td_elements[0].text,
													'Precio':td_elements[1].text})
							address_link = td_elements[0].find_elements_by_tag_name("a")
							link_address = address_link[0].get_attribute('href')
							self.driver_two.get(link_address)
							store_address = self.driver_two.find_element_by_id("lblDireccion")
							data_dictionary.update({'direccion':store_address.text})
							data_list.append(data_dictionary)
						else:
							self.kwargs['flag'] = True
						self.driver.switch_to.window(current_window)
					
					product.update({'places':data_list})
					self.callback_append(product)
					self.driver.quit()
					self.driver_two.quit()
				except Exception, e:
					#print e
					product = {}
			return

if __name__ == "__main__":
	profeco_agent = SeleniumProfeco('150901','1509010','atun')
	profeco_places = profeco_agent.run_agent()
	print profeco_places
