#!/usr/bin/python
# -*- encoding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

#from django.conf import settings

class settings(object):
	"""docstring for settings"""
	def __init__(self, arg):
		super(settings, self).__init__()
		self.arg = arg
	PROFECO_APP_URL_LOGIN = 'http://www.profeco.gob.mx/precios/canasta/loginUsu.aspx'
	PROFECO_APP_URL_SEARCH = 'http://www.profeco.gob.mx/precios/canasta/home.aspx?th=1'
	PROFECO_USERNAME = 'jl.mrtz@gmail.com'
	PROFECO_PASSWORD = 'Xilcom12'

import logging
import time
import threading

class Agent(object):
	"""docstring for Agent"""
	def __init__(self, *args):
		super(Agent, self).__init__()
		self.url_login = settings.PROFECO_APP_URL_LOGIN
		self.url_search = settings.PROFECO_APP_URL_SEARCH
		self.products_list = []
		self.frame_options = ''
		self.frame_results = ''

	def _create_agent(self,driver_type='firefox'):
		"""Create a new aget"""
		#print self._create_agent.__doc__
		if driver_type == 'firefox':
			self.driver = webdriver.Chrome()
		elif driver_type == 'headless':
			self.driver = webdriver.PhantomJS()#service_log_path=settings.PATH_LOG_PHANTOMJS)
		
	def _login_agent(self):
		self.driver.get(self.url_login)
		self._sigin_agent()
		
	def _go_to_search(self):
		self.driver.get(self.url_search)
		self.wait = WebDriverWait(self.driver,3)
		return self.driver

	def _sigin_agent(self):
		"""Log in agent for a search on detail product"""
		self.driver.find_element_by_id("txtUsuario").clear()
		self.driver.find_element_by_id("txtUsuario").send_keys(settings.PROFECO_USERNAME)
		self.driver.find_element_by_id("txtPassword").clear()
		self.driver.find_element_by_id("txtPassword").send_keys(settings.PROFECO_PASSWORD)
		self.driver.find_element_by_id("btnEnviar").click()

	def _select_city(self, select_id="cmbCiudad"):
		"""Select the city of the list"""
		#print self._select_city.__doc__
		self.frame_options = self.driver.find_elements_by_tag_name('iframe')[0]
		self.driver.switch_to.frame(self.frame_options);
		self.wait.until(lambda driver: self.driver.find_element_by_id(select_id))
		combo_city = self.driver.find_element_by_id(select_id)
		Select(combo_city).select_by_value(self.city)

	def _select_municipio(self, select_id="listaMunicipios", button_id="ImageButton1"):
		"""Select the municipio of the list"""
		#print self._select_municipio.__doc__
		self.wait.until(lambda driver: self.driver.find_element_by_id(select_id))
		select = self.driver.find_element_by_id(select_id)
		Select(select).select_by_value(self.municipio)
		button = self.driver.find_element_by_id(button_id)
		button.click()

	def _do_search(self, input_id="busq"):
		"""Make a search for product"""
		#print self._do_search.__doc__
		time.sleep(3)
		input_text = self.driver.find_element_by_id(input_id)
		input_text.send_keys(self.search)
		button_search = self.driver.find_element_by_id("IMG1")
		button_search.click()

	def _inspect_results(self, table_id="GridView1"):
		data_list, td_content = [], []
		self.driver.switch_to.default_content()
		self.frame_results = self.driver.find_elements_by_tag_name('iframe')[1]
		self.driver.switch_to.frame(self.frame_results);
		table = self.driver.find_element_by_id(table_id)
		tr_elements = table.find_elements_by_xpath(".//tr")

		return tr_elements

	def _start_agent(self):
		"""Run new Agent for new reach inside of the instance"""
		#print self._run_agent.__doc__, "\n"
		self._create_agent()
		self._login_agent()
		self._go_to_search()
		self._select_city()
		self._select_municipio()
		self._do_search()

	def _quit_agent(self):
			"""Quit agent"""
			self.driver.quit()
			self.__del__()

	def __del__(self):
		pass