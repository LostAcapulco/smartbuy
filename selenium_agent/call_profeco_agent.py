#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf import settings

from selenium_agent.agent_detail import Agent_detail
from selenium_agent.agent_general import Agent_general
from frsqr.views import *
from lib.helpers import *

class CallProfecoAgent(object):
	"""docstring for CallProfecoAgent"""
	def __init__(self, *args, **kwargs ):
		super(CallProfecoAgent, self).__init__()
		self.product = kwargs['product']
		self.address, = kwargs['adress']
		self.description = kwargs['description']
		self.product_detail = kwargs['product_detail']
		self.url = kwargs['url']

	def general_serach(self,  profeco_products=None):
		""" Search for a product in profeco webpage """
		if product is not None:
			product = normalize_search_product(self.product)
			address = normalize_search_address(self.address)
			address_city = [c for c in address if c in settings.PROFECO_STATES_CODE]
			address_city = address_city[0]
			profeco_agent = Agent_general(settings.PROFECO_STATES_CODE[address_city][0],
	                                settings.PROFECO_STATES_CODE[address_city][1],
	                                product)
		return profeco_agent.get_all_results()

	def detail_search(self):
		"""Search for a especific product"""
		address_city = [c for c in self.address if c in settings.PROFECO_STATES_CODE]
		address_city = address_city[0]
		profeco_agent = Agent_detail(settings.PROFECO_STATES_CODE[address_city][0],
		                        settings.PROFECO_STATES_CODE[address_city][1],
		                        self.product_detail,
		                        self.url,
		                        self.description)
		return profeco_agent.get_detail()

	def search_place(self, place, ltlng, four_places=None):
		""" Search for a place in foursuquare """
		if place is not None and ltlng is not None:
			four_places = search_places(place, ltlng)
		return four_places