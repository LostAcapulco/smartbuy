#!/usr/bin/python
# -*- encoding: utf-8 -*-

from agent import Agent

class Agent_general(Agent):
	"""docstring for Agent_general"""
	def __init__(self, *args):
		super(Agent_general, self).__init__()
		self.city = args[0]
		self.municipio = args[1]
		self.search = args[2]

	
	def get_all_results(self, table_id="GridView1", response=None,flag=False):
		"""Extrac the results from the table."""
		#print self.get_all_results.__doc__

		self._start_agent()
		data_list = []
		tr_elements = self._inspect_results()
		
		for tr_element in tr_elements:
			td_elements = tr_element.find_elements_by_tag_name("td")

			if flag:
				data_dictionary = {}
				description = td_elements[2].find_elements_by_tag_name("a")
				url = description[0].get_attribute('href')
				data_dictionary.update({'url':url,
									'description':description[0].text,
									'preciomin':td_elements[3].text,
									'preciomax':td_elements[4].text})
				data_list.append(data_dictionary)
			else:
				flag = True

		self._quit_agent()

		return data_list

if __name__ == '__main__':
	agent = Agent_general('150901','1509010','atun')
	print agent.get_all_results()
