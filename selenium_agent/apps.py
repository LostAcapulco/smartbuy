from __future__ import unicode_literals

from django.apps import AppConfig


class SeleniumAgentConfig(AppConfig):
    name = 'selenium_agent'
