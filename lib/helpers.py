#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import unicodedata

def normalize_search_product(product):
    """Change accent from letters to do a product over profeco site"""
    product  = ''.join(c for c in unicodedata.normalize('NFD', product)
                  if unicodedata.category(c) != 'Mn')
    product = product.replace('.','').replace('-',' ').replace("'","'").replace(',','')
    return product

def normalize_search_address(address):
    address = address.replace('.','').replace(' ','')
    address = ''.join(c for c in unicodedata.normalize('NFD', unicode(address)) if unicodedata.category(c) != 'Mn')
    address = address.lower().split(',')
    return address			