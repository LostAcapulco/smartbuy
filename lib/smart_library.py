from django.contrib.auth import authenticate, login, logout


def costum_login_require(function=None,user_pass_test=None,response=None):
	"""Check that a user is loged in the sistem"""
	decorator = user_pass_test(lambda u: u.is_authenticated(),response=response)
	if function:
		return decorator(function)

	return decorator
