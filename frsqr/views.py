#!/usr/bin/python
# -*- encoding: utf-8 -*-

from django.conf import settings

import foursquare as f_client
import json
import urllib
import os


def four_client():
    """foursquare client instance"""
    return f_client.Foursquare(client_id=settings.FOURSQUARE_CLIENT_ID, client_secret=settings.FOURSQUARE_CLIENT_SECRET)

def search_places(place=None, ltlng=None, result_place=None):
    """Search for nearest Coffe place"""
    if place is None and ltlng is None:
        place = "café"
        ltlng = "19.432722,-99.133201"
        print "creating client"
    client = four_client()
    result_place = client.venues.search(params={'ll':ltlng,'query': place})

    return json.dumps(result_place)

def search_tips(tip=None):
    """Search for tips like  donuts"""
    if tip is not None:
        client = four_client()
        r_tip = client.tips.search(params={'ll':'19.432722,-99.133201','query': tip})

    return r_tip

def search_venue_photo(venue_photo_request=None, r_venue_photo=None):
    """Search venue photos"""
    if venue_photo_request is not None:
        client = four_client()
        r_venue_photo = client.venues.photos()

    return r_venue_photo

def search_venue_events(venue_event_request=None, r_venue_events=None):
    """Search venue events"""
    if venue_event_request is not None:
        client = four_client()
        r_venue_events = client.venues.events()

    return r_venue_events

def explore_venue(place=None, r_place=None):
    """Explore for venues"""
    if place is not None:
        client = four_client()
        r_place = client.venues.explore()

    return r_place

def explore_section():
    """Explore for venues just a section"""
    client = four_client()
    r_section = client.venues.explore(params={'ll':'19.432722,-99.133201', 'section': u'café'})
    
    return r_section
        