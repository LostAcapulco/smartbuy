#!/usr/bin/python
# -*- encoding: utf-8 -*-
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from django.views.decorators.csrf import csrf_exempt

from selenium_agent.call_profeco_agent import CallProfecoAgent as AgentCall
from frsqr.views import *

@csrf_exempt
@api_view(['GET'])
def search_place(request, place, ltlng, four_places=None):
	""" Search for a place in foursuquare """
	if place is not None and ltlng is not None:
		four_places = search_places(place, ltlng)

	return Response(four_places)

@csrf_exempt
@api_view(['GET'])
def search_general(request, product, address):
    """ Search for a product in profeco webpage """
    profeco_products = AgentCall(product, address).general_serach()
    return Response(json.dumps(profeco_products))

@csrf_exempt
@api_view(['POST'])
def search_detail(request, profeco_places=None):
    """Search for a especific product"""
    url = request.POST.get('url')
    description = request.POST.get('description')
    product = request.POST.get('product')
    address = request.POST.get('address')
    profeco_places = AgentCall().detail_search(product=product,
                                            address=address,
                                            description=description)
    return Response(json.dumps(profeco_places))